%global _enable_debug_packages %{nil}
%global debug_package          %{nil}
%global __strip       /bin/true

Summary: Polish e-ID card and certificate manager
Summary(pl): e-dowód Menedżer karty i certyfikatów
Name: e-dowod
Version: 4.0.0.374
Release: 1
URL: https://www.gov.pl/web/e-dowod
Source0: https://www.gov.pl/pliki/edowod/e-dowod-%{version}.run
Source1: %{name}_manager.desktop
Source2: %{name}_monitor.desktop
License: PWPW
BuildRequires: desktop-file-utils
BuildRequires: %{_bindir}/icotool
BuildRequires: %{_bindir}/7za
BuildRequires: %{_bindir}/devtool-qt5
Provides: bundled(nss) = 3.39
Provides: bundled(spdlog)

%global __requires_exclude ^libCommon.so

%description -l pl
W ramach zainstalowanego oprogramowania e-Dowód, Użytkownik otrzymuje aplikację
Middleware, na którą składają się trzy programy do komunikacji z kartą i obsługi
dowodu z warstwą elektroniczną oraz program pomocy. Są to:
• e-dowód Monitor – służy do monitorowania czy karta jest umiejscowiona na
  czytniku. Działa w tle.
• e-dowód Podaj CAN – służy do podania kodu CAN i zestawienia połączenia.
  Uruchamia się po umiejscowieniu karty na czytniku i tylko wówczas, gdy
  aplikacja e-dowód Monitor jest uruchomiona.
• e-dowód Menedżer – służy do zarządzania kartą i znajdującymi się na niej
  certyfikatami. Uruchamiana jest z menu start przez Użytkownika.

%prep
%setup -cT
%{_bindir}/devtool-qt5 dump %{S:0} %{_builddir}/%{name}-%{version}

%build

%install
install -dm755 %{buildroot}%{_datadir}/icons
for component in can common core manager monitor ; do
    7za x -bd -o%{buildroot}%{_libdir}/%{name} metadata/64.${component}/%{version}content.7z
done
for component in can manager monitor ; do
    7za x -bd -o./ metadata/64.${component}/%{version}icons.7z
    icotool -x --icon --index=1 --width=256 icons/$(echo ${component} | tr 'cm' 'CM').ico
    install -pm644 $(echo ${component} | tr 'cm' 'CM')_1_256x256x32.png %{buildroot}%{_datadir}/icons/%{name}_${component}.png
done
7za x -bd -o%{buildroot}%{_libdir}/%{name} metadata/statics/%{version}statics.7z

install -dm755 %{buildroot}%{_libdir}/%{name}/Licenses
install -pm644 metadata/statics/license-e-dowod.html %{buildroot}%{_libdir}/%{name}/Licenses

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:2}
desktop-file-install --dir %{buildroot}%{_sysconfdir}/xdg/autostart %{S:2}

for lib in \
    libicu*.so.56 \
    libcrypto.so* \
    libssl.so* \
    libQt5Network.so.5 \
; do
    rm -v %{buildroot}%{_libdir}/%{name}/${lib}
done

%files
%lang(pl) %license %{_libdir}/%{name}/Licenses/license-e-dowod.html
%lang(pl) %doc %{_libdir}/%{name}/statics/instrukcja.pdf
%{_datadir}/applications/e-dowod_manager.desktop
%{_datadir}/applications/e-dowod_monitor.desktop
%{_sysconfdir}/xdg/autostart/e-dowod_monitor.desktop
%{_datadir}/icons/e-dowod_can.png
%{_datadir}/icons/e-dowod_manager.png
%{_datadir}/icons/e-dowod_monitor.png
%{_libdir}/%{name}/e-dowod-pkcs11-64.so
%{_libdir}/%{name}/e-dowod_can
%{_libdir}/%{name}/e-dowod_manager
%{_libdir}/%{name}/e-dowod_monitor
%{_libdir}/%{name}/libCommon.so
%{_libdir}/%{name}/nssdbm3_s.so
%{_libdir}/%{name}/statics/instrukcja.html
%dir %{_libdir}/%{name}/Licenses

%changelog
* Sat Jan 30 2021 Dominik Mierzejewski <rpm@greysector.net> 1.4.28-1
- initial build
